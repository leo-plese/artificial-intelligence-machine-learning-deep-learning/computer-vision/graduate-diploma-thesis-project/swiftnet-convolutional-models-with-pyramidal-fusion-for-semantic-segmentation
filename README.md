# SwiftNet Convolutional Models with Pyramidal Fusion for Semantic Segmentation

Project adjusted from original repository https://github.com/orsic/swiftnet according to needs of the research. Implemented in Python using PyTorch library.

Researching, training and redesigning these pyramidal SwiftNet convolutional models for semantic segmentation is part of my grad thesis deep learning project, which has the goal of comparing state-of-the-art transformer to convolutional models.

Training SwiftNet models and augmenting them using attention mechanism.

Project duration: Feb 2022 - July 2022.
