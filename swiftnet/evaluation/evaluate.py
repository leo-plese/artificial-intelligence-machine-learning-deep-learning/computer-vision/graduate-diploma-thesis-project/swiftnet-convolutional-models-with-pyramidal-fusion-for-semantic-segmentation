import contextlib

import numpy as np
import torch
from tqdm import tqdm
from time import perf_counter

import lib.cylib as cylib

__all__ = ['compute_errors', 'get_pred', 'evaluate_semseg']


def compute_errors(conf_mat, class_info, verbose=True):
    num_correct = conf_mat.trace()
    num_classes = conf_mat.shape[0]
    total_size = conf_mat.sum()
    avg_pixel_acc = num_correct / total_size * 100.0
    TPFP = conf_mat.sum(1)
    TPFN = conf_mat.sum(0)
    FN = TPFN - conf_mat.diagonal()
    FP = TPFP - conf_mat.diagonal()
    class_iou = np.zeros(num_classes)
    class_recall = np.zeros(num_classes)
    class_precision = np.zeros(num_classes)
    per_class_iou = []
    if verbose:
        print('Errors:')
    for i in range(num_classes):
        TP = conf_mat[i, i]
        class_iou[i] = (TP / (TP + FP[i] + FN[i])) * 100.0
        if TPFN[i] > 0:
            class_recall[i] = (TP / TPFN[i]) * 100.0
        else:
            class_recall[i] = 0
        if TPFP[i] > 0:
            class_precision[i] = (TP / TPFP[i]) * 100.0
        else:
            class_precision[i] = 0

        class_name = class_info[i]
        per_class_iou += [(class_name, class_iou[i])]
        if verbose:
            print('\t%s IoU accuracy = %.2f %%' % (class_name, class_iou[i]))
    avg_class_iou = class_iou.mean()
    avg_class_recall = class_recall.mean()
    avg_class_precision = class_precision.mean()
    if verbose:
        print('IoU mean class accuracy -> TP / (TP+FN+FP) = %.2f %%' % avg_class_iou)
        print('mean class recall -> TP / (TP+FN) = %.2f %%' % avg_class_recall)
        print('mean class precision -> TP / (TP+FP) = %.2f %%' % avg_class_precision)
        print('pixel accuracy = %.2f %%' % avg_pixel_acc)
    return avg_pixel_acc, avg_class_iou, avg_class_recall, avg_class_precision, total_size, per_class_iou


def get_pred(logits, labels, conf_mat):
    _, pred = torch.max(logits.data, dim=1)
    pred = pred.byte().cpu()
    pred = pred.numpy().astype(np.int32)
    true = labels.numpy().astype(np.int32)
    cylib.collect_confusion_matrix(pred.reshape(-1), true.reshape(-1), conf_mat)


def mt(sync=False):
    if sync:
        torch.cuda.synchronize()
    return 1000 * perf_counter()


def evaluate_semseg(model, data_loader, class_info, multiscale=False, observers=()):
    model.eval()

    im_h,im_w=1024,2048
    if multiscale:
        from torchvision.transforms import Resize
        scale_factors = [0.5,0.75,1.0,1.25,1.5,1.75,]
        resizes = [Resize((int(f*im_h),int(f*im_w))) for f in scale_factors]

    print("model device =", next(model.parameters()).device)

    model_dev = next(model.parameters()).device
    im_h,im_w = 1024,2048
    managers = [torch.no_grad()] + list(observers)
    with contextlib.ExitStack() as stack:
        for ctx_mgr in managers:
            stack.enter_context(ctx_mgr)
        conf_mat = np.zeros((model.num_classes, model.num_classes), dtype=np.uint64)
        for step, batch in tqdm(enumerate(data_loader), total=len(data_loader)):
            #print("========BTC========")
            #print(batch["image"].shape)
            #print(batch["image"])
            im = batch["image"].to(model_dev)
            #im_h, im_w = im.shape[-2:]
            ##print("=========",im_h,im_w,im.device,next(model.parameters()).device)
            ims = []
            if multiscale:
                ##print("ORIG", im.shape)
                ims = [r(im) for r in resizes]
                ##print("RESIZED", [im.shape for im in ims])
                ims += [torch.flip(im, (-1,)) for im in ims]
                ##print("FLIPPED", [im.shape for im in ims])
            else:
                ims.append(im)

            logits_sum = torch.zeros((1, model.num_classes, im_h, im_w), device=im.device)
            ##print("LOGITS SUM",logits_sum.shape)
            
            batch['original_labels'] = batch['original_labels'].numpy().astype(np.uint32)
            ##print("aft orig lbs", batch['original_labels'].shape)

            batch_im = batch["image"]
            im_num = 0
            for im in ims:
                batch["image"] = im
                #print("BATCH IM......",batch["image"].shape)
                logits, additional = model.do_forward(batch, batch['original_labels'].shape[1:3])
                ##print("LOGITS",logits.shape)
                if im_num >= 6:
                    logits = torch.flip(logits,(-1,))
                logits_sum += logits
                im_num += 1


            batch["image"] = batch_im
            ##print("LENS",len(ims),len(logits_sum))
            logits_sum /= len(ims)
            pred = torch.argmax(logits_sum.data, dim=1).byte().cpu().numpy().astype(np.uint32)
            for o in observers:
                o(pred, batch, additional)
            cylib.collect_confusion_matrix(pred.flatten(), batch['original_labels'].flatten(), conf_mat)
        print('')
        pixel_acc, iou_acc, recall, precision, _, per_class_iou = compute_errors(conf_mat, class_info, verbose=True)
    model.train()
    return iou_acc, per_class_iou
