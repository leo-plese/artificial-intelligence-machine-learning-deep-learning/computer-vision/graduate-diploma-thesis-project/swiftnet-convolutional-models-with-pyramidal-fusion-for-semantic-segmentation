import argparse
from pathlib import Path
import importlib.util
from evaluation import evaluate_semseg
import torch


def import_module(path):
    spec = importlib.util.spec_from_file_location("module", path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module

def do_eval_step(
    model,
    device,
    im_sh,
    target_size_feats=None,
    bs=None
):
    ##print("DEV=========",device)
    torch.cuda.empty_cache()
    im = torch.randint(low=0, high=256, size=(bs, 3, *im_sh)).float().to(device) #float32

    logits = model.forward(im, target_size_feats, im_sh, ret_add=False)
    pred = torch.argmax(logits, dim=1)

    del logits, pred

    torch.cuda.synchronize()

def get_max_inputsize_eval(
    model,
    init_im_sh=(64,128),
    init_target_size_feats=(16,32),
):
    model.eval()

    model_dev = next(model.parameters()).device

    double_dim = lambda shape_tup : tuple(2*d for d in shape_tup)
    half_dim = lambda shape_tup: tuple(d//2 for d in shape_tup)

    factor_feats = init_im_sh[0] // init_target_size_feats[0]
    scale_feats = lambda shape_tup: tuple(d//factor_feats for d in shape_tup)

    with torch.no_grad():
        cur_im_sh, cur_target_size_feats = init_im_sh, init_target_size_feats
        while True:
            try:
                print("cur_im_sh =", cur_im_sh, cur_target_size_feats)
                do_eval_step(
                    model,
                    model_dev,
                    im_sh=cur_im_sh,
                    target_size_feats=cur_target_size_feats,
                    bs=1,
                )

                cur_im_sh = double_dim(cur_im_sh)
                cur_target_size_feats = scale_feats(cur_im_sh)
            except RuntimeError as err:
                print("!!! err =", err)
                top_im_sh, top_target_size_feats = cur_im_sh, cur_target_size_feats
                bottom_im_sh = half_dim(cur_im_sh)
                bottom_target_size_feats = scale_feats(bottom_im_sh)
                break

        print("bottom =", bottom_im_sh, bottom_target_size_feats)
        print("top =", top_im_sh, top_target_size_feats)
        while (top_im_sh[0] - bottom_im_sh[0]) > 1:
            mid_im_sh = half_dim(tuple(d1+d2 for d1,d2 in zip(top_im_sh, bottom_im_sh)))
            mid_target_size_feats = scale_feats(mid_im_sh)
            print("mid_im_sh =", mid_im_sh, mid_target_size_feats)
            try:
                do_eval_step(
                    model,
                    model_dev,
                    im_sh=mid_im_sh,
                    target_size_feats=mid_target_size_feats,
                    bs=1,
                )
                bottom_im_sh, bottom_target_size_feats = mid_im_sh, mid_target_size_feats
            except RuntimeError:
                top_im_sh, top_target_size_feats = mid_im_sh, mid_target_size_feats

        print("bottom =", bottom_im_sh, bottom_target_size_feats)
        print("top =", top_im_sh, top_target_size_feats)
        try:
            do_eval_step(
                model,
                model_dev,
                im_sh=top_im_sh,
                target_size_feats=top_target_size_feats,
                bs=1,
            )
            max_im_sh = top_im_sh
        except RuntimeError as err:
            print("!!! err =", err)
            max_im_sh = bottom_im_sh

    return max_im_sh

def get_max_batchsize_eval(
    model,
    im_sh=(1024,2048),
    target_size_feats=(256,512),
):
    model.eval()

    model_dev = next(model.parameters()).device

    with torch.no_grad():
        cur_bs = 2
        while True:
            try:
                print("cur_bs =", cur_bs)
                do_eval_step(
                    model,
                    model_dev,
                    im_sh,
                    target_size_feats=target_size_feats,
                    bs=cur_bs,
                )

                cur_bs *= 2
            except RuntimeError as err:
                print("!!! err =", err)
                top_bs = cur_bs
                bottom_bs = cur_bs // 2
                break

        print("bottom =", bottom_bs)
        print("top =", top_bs)
        while (top_bs - bottom_bs) > 1:
            mid_bs = (top_bs + bottom_bs) // 2
            print("mid_bs =", mid_bs)
            try:
                do_eval_step(
                    model,
                    model_dev,
                    im_sh,
                    target_size_feats=target_size_feats,
                    bs=mid_bs,
                )
                bottom_bs = mid_bs
            except RuntimeError:
                top_bs = mid_bs

        print("bottom =", bottom_bs)
        print("top =", top_bs)
        try:
            do_eval_step(
                model,
                model_dev,
                im_sh,
                target_size_feats=target_size_feats,
                bs=top_bs,
            )
            max_bs = top_bs
        except RuntimeError as err:
            print("!!! err =", err)
            max_bs = bottom_bs

    return max_bs

def measure_speed_eval_fps(
    model,
    bs,
    im_sh=(1024,2048),
    target_size_feats=(256,512),
    n=100):

    import torch
    import time

    model.eval()


    warm_up = 10

    model_dev = next(model.parameters()).device

    ###############
    torch.cuda.empty_cache()
    im = torch.randint(low=0, high=256, size=(bs, 3, *im_sh)).float().to(model_dev) # float32
    ###############

    with torch.no_grad():
        for i in range(warm_up):
            torch.cuda.empty_cache()
            #print(i)
            logits = model.forward(im, target_size_feats, im_sh, ret_add=False)
            pred = torch.argmax(logits.data, dim=1)
            del logits, pred

    tot_time = 0
    with torch.no_grad():
        for _ in range(n):
            torch.cuda.empty_cache()

            torch.cuda.synchronize()
            start = time.time()

            logits = model.forward(im, target_size_feats, im_sh, ret_add=False)
            pred = torch.argmax(logits, dim=1)

            torch.cuda.synchronize()
            end = time.time()

            tot_time += (end - start)

            del logits, pred

    return (bs * n) / tot_time


def measure_speed_eval_wrt_inputsize(
        model,
        max_im_sh=(1024, 2048),
        max_target_size_feats=(256,512),
        n=100):
    im_sh = (64, 128)
    max_im_h, max_im_w = max_im_sh
    double_dim = lambda shape_tup: tuple(2 * d for d in shape_tup)

    factor_feats = max_im_sh[0] // max_target_size_feats[0]
    scale_feats = lambda shape_tup: tuple(d//factor_feats for d in shape_tup)

    while im_sh[0] < max_im_h:
        print("im_sh =", im_sh)
        speed_result = measure_speed_eval_fps(
            model,
            bs=1,
            im_sh=im_sh,
            target_size_feats=scale_feats(im_sh),
            n=n)
        print("FPS = ", speed_result)

        im_sh = double_dim(im_sh)

    print("im_sh = MAX IM SH =", max_im_sh)
    speed_result = measure_speed_eval_fps(
        model,
        bs=1,
        im_sh=max_im_sh,
        target_size_feats=max_target_size_feats,
        n=n)
    print("FPS = ", speed_result)


parser = argparse.ArgumentParser(description='Detector train')
parser.add_argument('config', type=str, help='Path to configuration .py file')
parser.add_argument('--multiscale', type=int, default=0)
parser.add_argument('--profile', dest='profile', action='store_true', help='Profile one forward pass')
parser.add_argument('--gpu-id', dest='gpu_id', type=int, default=0)
parser.add_argument("--get-max-bs-eval", type=int, default=0)
parser.add_argument("--measure-speed-eval", type=int, default=0)
parser.add_argument("--measure-speed-eval-batchsize", type=int, default=None)
parser.add_argument("--get-max-inputsize-eval", type=int, default=0)
parser.add_argument("--measure-speed-eval-wrt-inputsize", type=int, default=0)  # dim h - from im_sh = (h, w); w = 2*h
parser.add_argument("--measure-speed-eval-wrt-inputsize-maxinputsize", type=int, default=None)
parser.add_argument("--measure-speed-eval-wrt-inputsize-iters", type=int, default=100)

if __name__ == '__main__':
    args = parser.parse_args()
    conf_path = Path(args.config)
    conf = import_module(args.config)

    class_info = conf.dataset_val.class_info

    model = conf.model.cuda(args.gpu_id)

    if args.get_max_bs_eval:
        max_bs_result = get_max_batchsize_eval(conf.model)
        print("MAX BS =", max_bs_result)
    elif args.measure_speed_eval:
        speed_result = measure_speed_eval_fps(conf.model, args.measure_speed_eval_batchsize)
        print("FPS = ", speed_result)
    elif args.get_max_inputsize_eval:
        max_inputsize_result = get_max_inputsize_eval(conf.model)
        print("MAX INPUTSIZE =", max_inputsize_result)
    elif args.measure_speed_eval_wrt_inputsize:
        max_im_h, max_im_w = args.measure_speed_eval_wrt_inputsize_maxinputsize, 2*args.measure_speed_eval_wrt_inputsize_maxinputsize
        measure_speed_eval_wrt_inputsize(conf.model, max_im_sh=(max_im_h, max_im_w),
                                         max_target_size_feats=(max_im_h//4, max_im_w//4),
                                         n=args.measure_speed_eval_wrt_inputsize_iters)
    else:
        for loader, name in conf.eval_loaders:
            iou, per_class_iou = evaluate_semseg(model, loader, class_info, multiscale=(args.multiscale==1), observers=conf.eval_observers)
            print(f'{name}: {iou:.2f}')
