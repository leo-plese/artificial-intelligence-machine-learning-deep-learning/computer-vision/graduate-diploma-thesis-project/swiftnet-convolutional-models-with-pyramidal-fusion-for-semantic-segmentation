import argparse
import os
from pathlib import Path
import torch
import importlib.util
import datetime
import sys
from shutil import copy
import pickle
from time import perf_counter

from evaluation import evaluate_semseg


def import_module(path):
    spec = importlib.util.spec_from_file_location("module", path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module


def store(model, store_path, name):
    with open(store_path.format(name), 'wb') as f:
        torch.save(model.state_dict(), f)

# LEO - BEG
def load(model, store_path, name):
    with open(store_path.format(name), 'rb') as f:
        model.load_state_dict(torch.load(f))
# LEO - END

class Logger(object):
    def __init__(self, *files):
        self.files = files

    def write(self, obj):
        for f in self.files:
            f.write(obj)
            f.flush()  # If you want the output to be visible immediately

    def flush(self):
        for f in self.files:
            f.flush()


class Trainer:
    def __init__(self, conf, args, name):
        self.conf = conf
        using_hparams = hasattr(conf, 'hyperparams')
        print(f'Using hparams: {using_hparams}')
        self.hyperparams = self.conf
        self.args = args
        self.name = name
        self.model = self.conf.model
        self.optimizer = self.conf.optimizer

        self.dataset_train = self.conf.dataset_train
        self.dataset_val = self.conf.dataset_val
        self.loader_train = self.conf.loader_train
        self.loader_val = self.conf.loader_val

    def __enter__(self):
        self.best_iou = -1
        self.best_iou_epoch = -1
        self.validation_ious = []
        self.experiment_start = datetime.datetime.now()

        if self.args.resume:
            self.experiment_dir = Path(self.args.resume)
            print(f'Resuming experiment from {args.resume}')
        else:
            self.experiment_dir = Path(self.args.store_dir) / (
                    self.experiment_start.strftime('%Y_%m_%d_%H_%M_%S_') + self.name)

        self.checkpoint_dir = self.experiment_dir / 'stored'
        self.store_path = str(self.checkpoint_dir / '{}.pt')

        if not self.args.dry and not self.args.resume:
            os.makedirs(str(self.experiment_dir), exist_ok=True)
            os.makedirs(str(self.checkpoint_dir), exist_ok=True)
            copy(self.args.config, str(self.experiment_dir / 'config.py'))

        if self.args.log and not self.args.dry:
            f = (self.experiment_dir / 'log.txt').open(mode='a')
            sys.stdout = Logger(sys.stdout, f)

        self.model.cuda(self.args.gpu_id)

        if self.args.resume:
            load(self.model, self.store_path, 'model')
            load(self.optimizer, self.store_path, 'optimizer')
            ###load(self.conf.lr_scheduler, self.store_path, 'lrscheduler')

        #self.model.cuda()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not self.args.dry:
            store(self.model, self.store_path, 'model')
            store(self.optimizer, self.store_path, 'optimizer')
            ####store(self.conf.lr_scheduler, self.store_path, 'lrscheduler')

        if not self.args.dry:
            with open(f'{self.experiment_dir}/val_ious.pkl', 'wb') as f:
                pickle.dump(self.validation_ious, f)
            dir_iou = Path(self.args.store_dir) / (f'{self.best_iou:.2f}_'.replace('.', '-') + self.name)
            os.rename(self.experiment_dir, dir_iou)

    def train(self):
        print("ARGS:GPU =", self.args.gpu_id)
        print("model device =", next(self.model.parameters()).device)

        num_epochs = self.hyperparams.epochs
        start_epoch = self.hyperparams.start_epoch if hasattr(self.hyperparams, 'start_epoch') else 0
        if start_epoch != 0:
            for i in range(start_epoch):
                self.conf.lr_scheduler.step()
        for epoch in range(start_epoch, num_epochs):
            torch.cuda.empty_cache()  # LEO - ADD
            if hasattr(self.conf, 'epoch'):
                self.conf.epoch.value = epoch
                print(self.conf.epoch)
            self.model.train()
            try:
                self.conf.lr_scheduler.step()
                print(f'Elapsed time: {datetime.datetime.now() - self.experiment_start}')
                for group in self.optimizer.param_groups:
                    print('LR: {:.4e}'.format(group['lr']))
                eval_epoch = ((epoch % self.conf.eval_each == 0) or (epoch == num_epochs - 1))  # and (epoch > 0)
                self.model.criterion.step_counter = 0
                print(f'Epoch: {epoch} / {num_epochs - 1}')
                if eval_epoch and not self.args.dry:
                    print("Experiment dir: %s" % self.experiment_dir)
                batch_iterator = iter(enumerate(self.loader_train))
                start_t = perf_counter()
                for step, batch in batch_iterator:
                    #print("=======BTC=======")
                    #print(batch["image"].shape)
                    torch.cuda.empty_cache() ######
                    self.optimizer.zero_grad()
                    loss = self.model.loss(batch)
                    # torch.cuda.empty_cache()  # LEO - ADD
                    loss.backward()
                    self.optimizer.step()
                    if step % 80 == 0 and step > 0:
                        curr_t = perf_counter()
                        print(f'{(step * self.conf.batch_size) / (curr_t - start_t):.2f}fps')
                if not self.args.dry:
                    store(self.model, self.store_path, 'model')
                    store(self.optimizer, self.store_path, 'optimizer')
                    ###store(self.conf.lr_scheduler, self.store_path, 'lrscheduler')
                if eval_epoch and self.args.eval:
                    print('Evaluating model')
                    iou, per_class_iou = evaluate_semseg(self.model, self.loader_val, self.dataset_val.class_info)
                    self.validation_ious += [iou]
                    if self.args.eval_train:
                        print('Evaluating train')
                        evaluate_semseg(self.model, self.loader_train, self.dataset_train.class_info)
                    if iou > self.best_iou:
                        self.best_iou = iou
                        self.best_iou_epoch = epoch
                        if not self.args.dry:
                            copy(self.store_path.format('model'), self.store_path.format('model_best'))
                    print(f'Best mIoU: {self.best_iou:.2f}% (epoch {self.best_iou_epoch})')

            except KeyboardInterrupt:
                break


def do_train_step(
    model,
    optimizer,
    im_sh=(1024, 2048),
    crop_size=768,
    num_classes=19,
    bs=None,
    batch=None,
    dist_trans_alphas=None,
    record_memory=False
):
    ##print("DEV=========",device)
    #print("crop cls",crop_size,num_classes)
    #print("N CLS",model.n_cls,num_classes,"CROPSIZE",crop_size)#,im.shape,seg_gt.shape)
    if batch is None:
        torch.cuda.empty_cache()
        batch = {}
        batch['original_labels'] = torch.randint(low=0, high=num_classes+1, size=(bs, *im_sh))   #int64
        batch['labels'] = torch.randint(low=0, high=num_classes+1, size=(bs, crop_size, crop_size))   #int64
        batch['target_size'] = [crop_size,crop_size]
        batch['image'] = torch.randint(low=0, high=256, size=(bs, 3, crop_size, crop_size)).float() #float32

        if dist_trans_alphas is not None:
            section = crop_size//(len(dist_trans_alphas)+1)
            batch['label_distance_alphas'] = torch.zeros((bs, crop_size, crop_size),dtype=torch.float32) #float32
            for i in range(len(dist_trans_alphas)):
                batch['label_distance_alphas'][:,i*section:(i+1)*section,...] = dist_trans_alphas[i]

    #for k in batch:
    #    if torch.is_tensor(batch[k]):
    #        print(k,":",batch[k].device,batch[k].shape,batch[k].dtype,torch.unique(batch[k]),torch.min(batch[k]),torch.max(batch[k]))
    #    else:
    #        print(k,":",batch[k])
    #print("im",batch["image"].shape)
    optimizer.zero_grad()
    loss = model.loss(batch)

    loss.backward()

    mem_bwd = torch.cuda.max_memory_allocated()
    #print("mem_bwd", mem_bwd)
    torch.cuda.reset_max_memory_allocated()

    #print("mem 2", torch.cuda.max_memory_allocated())
    optimizer.step()
    #print("mem 3", torch.cuda.max_memory_allocated())

    del loss
    torch.cuda.synchronize()

    if record_memory:
        max_mem = torch.cuda.max_memory_allocated(model_dev)
        return max_mem, mem_bwd - max_mem


def get_max_batchsize_train(
    model,
    optimizer,
    dist_trans_alphas=None,
    im_sh=(1024,2048),
    crop_size=768,
    num_classes=19
):
    model.train()

    model_dev = next(model.parameters()).device

    cur_bs = 2
    while True:
        try:
            print("cur_bs =", cur_bs)
            do_train_step(
                model,
                optimizer,
                im_sh,
                crop_size,
                num_classes,
                bs=cur_bs,
                dist_trans_alphas=dist_trans_alphas,
            )

            cur_bs *= 2
        except RuntimeError as err:
            print("!!! err =", err)
            top_bs = cur_bs
            bottom_bs = cur_bs // 2
            break

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    while (top_bs - bottom_bs) > 1:
        mid_bs = (top_bs + bottom_bs) // 2
        print("mid_bs =", mid_bs)
        try:
            do_train_step(
                model,
                optimizer,
                im_sh,
                crop_size,
                num_classes,
                bs=mid_bs,
                dist_trans_alphas=dist_trans_alphas,
            )
            bottom_bs = mid_bs
        except RuntimeError:
            top_bs = mid_bs

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    try:
        do_train_step(
            model,
            optimizer,
            im_sh,
            crop_size,
            num_classes,
            bs=top_bs,
            dist_trans_alphas=dist_trans_alphas,
        )
        max_bs = top_bs
    except RuntimeError as err:
        print("!!! err =", err)
        max_bs = bottom_bs

    return max_bs

def measure_speed_train_fps(model,
    bs,
    optimizer,
    dist_trans_alphas=None,
    im_sh=(1024,2048),
    crop_size=768,
    num_classes=19,
    n=30):

    import torch
    import time

    model.train()

    model_dev = next(model.parameters()).device

    warm_up = 10

    ###############
    torch.cuda.empty_cache()
    batch = {}
    batch['original_labels'] = torch.randint(low=0, high=num_classes + 1, size=(bs, *im_sh))  # int64
    batch['labels'] = torch.randint(low=0, high=num_classes + 1, size=(bs, crop_size, crop_size))  # int64
    batch['target_size'] = [crop_size, crop_size]
    batch['image'] = torch.randint(low=0, high=256, size=(bs, 3, crop_size, crop_size)).float()  # float32

    if dist_trans_alphas is not None:
        section = crop_size // (len(dist_trans_alphas) + 1)
        batch['label_distance_alphas'] = torch.zeros((bs, crop_size, crop_size), dtype=torch.float32)  # float32
        for i in range(len(dist_trans_alphas)):
            batch['label_distance_alphas'][:, i * section:(i + 1) * section, ...] = dist_trans_alphas[i]
    ###############

    torch.cuda.synchronize()
    for i in range(warm_up):
        torch.cuda.empty_cache()
        print("i",i)
        do_train_step(
            model,
            optimizer,
            im_sh,
            crop_size,
            num_classes,
            batch=batch,
            dist_trans_alphas=dist_trans_alphas,
        )

    start = time.time()

    for i in range(n):
        torch.cuda.empty_cache()
        do_train_step(
            model,
            optimizer,
            im_sh,
            crop_size,
            num_classes,
            batch=batch,
            dist_trans_alphas=dist_trans_alphas,
        )

    torch.cuda.synchronize()

    end = time.time()
    return (bs * n) / (end - start)

def train_one_epoch_duration(
    model,
    optimizer,
    data_loader
):
    batch_iterator = iter(enumerate(data_loader))

    for i, batch in batch_iterator:
        if i % 50 == 0:
            print("i", i)

        torch.cuda.empty_cache()
        optimizer.zero_grad()
        loss = model.loss(batch)

        loss.backward()
        optimizer.step()

        del loss

        torch.cuda.synchronize()

def measure_macs_params(model):
    print("model.training", model.training)
    from thop import profile
    # from torchvision.models import resnet101
    # model = resnet101().to(ptu.device)
    model.eval()
    print("tr=", model.training)
    inp = torch.randn(1, 3, 1024, 2048).to(next(model.parameters()).device)
    macs, params = profile(model, inputs=(inp,(1024,2048),(1024,2048)))

    return macs, params

parser = argparse.ArgumentParser(description='Detector train')
parser.add_argument('config', type=str, help='Path to configuration .py file')
parser.add_argument('--store_dir', default='saves/', type=str, help='Path to experiments directory')
parser.add_argument('--resume', default=None, type=str, help='Path to existing experiment dir')
parser.add_argument('--no-log', dest='log', action='store_false', help='Turn off logging')
parser.add_argument('--log', dest='log', action='store_true', help='Turn on train evaluation')
parser.add_argument('--no-eval-train', dest='eval_train', action='store_false', help='Turn off train evaluation')
parser.add_argument('--eval-train', dest='eval_train', action='store_true', help='Turn on train evaluation')
parser.add_argument('--no-eval', dest='eval', action='store_false', help='Turn off evaluation')
parser.add_argument('--eval', dest='eval', action='store_true', help='Turn on evaluation')
parser.add_argument('--dry-run', dest='dry', action='store_true', help='Don\'t store')
parser.add_argument('--only-load-model', dest='only_load_model', default=0, type=int)
parser.add_argument('--gpu-id', dest='gpu_id', type=int, default=0)
parser.add_argument("--get-max-bs-train", type=int, default=0)
parser.add_argument("--measure-speed-train", type=int, default=0)
parser.add_argument("--measure-speed-train-batchsize", type=int, default=None)
parser.add_argument("--measure-train-duration", type=int, default=0)
parser.add_argument("--measure-duration-train-batchsize", type=int, default=None)
parser.add_argument("--n-epoch", type=int, default=1)
parser.add_argument("--log-memory", type=int, default=0)
parser.add_argument("--log-memory-batchsize", type=int, default=None)
parser.add_argument("--measure-macs", type=int, default=0)

parser.set_defaults(log=True)
parser.set_defaults(eval_train=False)
parser.set_defaults(eval=True)

if __name__ == '__main__':
    args = parser.parse_args()
    conf_path = Path(args.config)
    conf = import_module(args.config)

    if args.get_max_bs_train:
        conf.model.cuda(args.gpu_id)
        if "pyramid" in args.config:
            max_bs_result = get_max_batchsize_train(conf.model, conf.optimizer, conf.dist_trans_alphas)
        else:
            max_bs_result = get_max_batchsize_train(conf.model, conf.optimizer)
        print("MAX BS =", max_bs_result)
    elif args.measure_speed_train:
        conf.model.cuda(args.gpu_id)
        if "pyramid" in args.config:
            speed_result = measure_speed_train_fps(conf.model, args.measure_speed_train_batchsize, conf.optimizer, conf.dist_trans_alphas)
        else:
            speed_result = measure_speed_train_fps(conf.model, args.measure_speed_train_batchsize, conf.optimizer)
        print("FPS = ", speed_result)
    elif args.measure_train_duration:
        import time
        from torch.utils.data import DataLoader
        from data.transform import custom_collate

        if "pyramid" in args.config:
            conf.loader_train = DataLoader(conf.dataset_train, batch_size=args.measure_duration_train_batchsize, num_workers=conf.nw, pin_memory=True, drop_last=True, collate_fn=custom_collate, shuffle=True)
        else:
            conf.loader_train = DataLoader(conf.dataset_train, batch_size=args.measure_duration_train_batchsize, shuffle=True, num_workers=4, pin_memory=True, drop_last=True, collate_fn=custom_collate)

        conf.model.cuda(args.gpu_id)

        conf.model.train()

        epoch_times = []
        print("BATCH_SIZE===", conf.loader_train.batch_size)
        print("*************** NUM EPOCHS =", args.n_epoch, "***************")
        for ep in range(args.n_epoch):
            torch.cuda.synchronize()
            start = time.time()

            # train for one epoch
            train_one_epoch_duration(conf.model, conf.optimizer, conf.loader_train)

            torch.cuda.synchronize()
            end = time.time()

            epoch_time = (end - start)
            print(">>>>>>>>>>>>>>> TIME =", epoch_time, ">>>>>>>>>>>>>>>")

            epoch_times.append(epoch_time)

        avg_epoch_time = 0.0
        for i in range(args.n_epoch):
            print("epoch {} : {}".format(i, epoch_times[i]))
            avg_epoch_time += epoch_times[i]
        avg_epoch_time /= args.n_epoch
        print(">>>>>>>>>>>>>> AVERAGE EPOCH TIME =", avg_epoch_time, ">>>>>>>>>>>>>>")
    elif args.log_memory:
        conf.model.cuda(args.gpu_id)

        conf.model.train()

        mem_alloc_before_train = torch.cuda.memory_allocated(args.gpu_id)
        print("MEMORY allocated before TRAIN:", mem_alloc_before_train)

        model_dev = next(conf.model.parameters()).device

        print("BATCHSIZE======",args.log_memory_batchsize)
        dist_trans_alphas = conf.dist_trans_alphas if "pyramid" in args.config else None
        for i in range(10):
            param_moments_mem, activations_mem = do_train_step(
                conf.model,
                conf.optimizer,
                im_sh=(1024, 2048),
                crop_size=768,
                num_classes=19,
                bs=args.log_memory_batchsize,
                dist_trans_alphas=dist_trans_alphas,
                record_memory=True
            )

        print("MEMORY:")
        print("mem alloc before train:", mem_alloc_before_train)
        GB = 1024.0 * 1024.0 * 1024.0
        #param_moments_mem, activations_mem = (max_mem_alloc_during_train - mem_alloc_before_train) #/ args.batch_size_for_memory
        print("MEMORY REQUIREMENTS FOR TRAIN: (PARAMS + MOMENTS, ACTIVATIONS) = ({}, {}) =({} GB, {} GB)".format(param_moments_mem, activations_mem, param_moments_mem / GB, activations_mem / GB))
    elif args.measure_macs:
        macs, params = measure_macs_params(conf.model)

        print("MACS", macs)
        print("PARAMS", params)
    else:
        with Trainer(conf, args, conf_path.stem) as trainer:
            if args.only_load_model == 0:
                print("HERE 0000")
                trainer.train()
            else:
                print("HERE 1111")
                store(conf.model, str(Path(args.store_dir) / '{}.pt'), 'model')
